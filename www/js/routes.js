angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('Semiologia', {
    url: '/main',
    templateUrl: 'templates/Semiologia.html',
    controller: 'SemiologiaCtrl'
  })

  .state('sistemaNervoso', {
    url: '/p3',
    templateUrl: 'templates/sistemaNervoso.html',
    controller: 'sistemaNervosoCtrl'
  })

  .state('sistemaRespiratRio', {
    url: '/p4',
    templateUrl: 'templates/sistemaRespiratRio.html',
    controller: 'sistemaRespiratRioCtrl'
  })

  .state('sistemaOsteomuscular', {
    url: '/p5',
    templateUrl: 'templates/sistemaOsteomuscular.html',
    controller: 'sistemaOsteomuscularCtrl'
  })

  .state('sistemaCardiovacular', {
    url: '/p2',
    templateUrl: 'templates/sistemaCardiovacular.html',
    controller: 'sistemaCardiovacularCtrl'
  })

$urlRouterProvider.otherwise('/main')

  

});